// soal 1
var ulang = 1;
var ulang2 = 20;
console.log ('LOOPING PERTAMA')
 while(ulang<=ulang2){
   if (ulang%2==0)
  console.log(ulang+'-I love coding');
  ulang++;
 }

var x = 20;
var y = 1;
console.log ('LOOPING KEDUA')
 while(x>=y){
   if (x%2==0)
  console.log(x+'-I will become a frontend developer');
  x--;
 }

 console.log('--------------------')

// soal 2
for(var angka = 1; angka <= 20; angka++){
    if((angka%2)==1){
      if ((angka%3)===0) {
        console.log(angka+'-I love coding');
      }
      else{
          console.log(angka+'-Woles');
      }  
    }
    else if((angka%2)===0) {
      console.log(angka+'-Paham');
    }
  }

  console.log('--------------------')

// soal 3
function segitiga(panjang) {
    let hasil = '';
    for (let i = 0; i < panjang; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += ' # ';
        }
        hasil += '\n ';
    }
    return hasil;
}
console.log(segitiga(7));
console.log('--------------------')

// soal 4
var kalimat="saya sangat senang belajar javascript";
var nama=kalimat.split(' ');
console.log(nama)
console.log('--------------------')

// soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var urutan =daftarBuah.sort();
var urutan2=urutan.length;
var txt = "";
for (var i=0;i<urutan2;i++){
  txt += urutan[i]+'\n';
}
console.log(txt);
console.log('--------------------')

