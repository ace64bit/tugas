//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
//var upper =kataKeempat.toUpperCase();
console.log(kataPertama.concat(" ",kataKedua," ",kataKetiga," ", kataKeempat.toUpperCase())); //saya senang belajar javascript
console.log ('-------------------------');

//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var number1 = Number("1");
var number2 = Number("2");
var number3 = Number("4");
var number4 = Number("5");
console.log (number1,number2,number3,number4)
console.log (number1+number2+number3+number4)
console.log ('-------------------------');

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua= kalimat.substring(3, 15);
var kataKetiga= kalimat.substring(15, 18);
var kataKeempat= kalimat.substring(18, 24);
var kataKelima= kalimat.substring(24, 31);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
console.log ('-------------------------');

// soal 4
var nilai=80;
  
if (nilai >= 80)
{
   console.log("A");
}
else if (nilai >= 70)
{
   console.log("B");
}
else if (nilai >= 60)
{
   console.log("C");
}
else if (nilai >= 50)
{
   console.log("D");
}
else if (nilai < 50)
{
   console.log("E");
}
console.log ('-------------------------');

// soal 5
var tanggal = 10;
var bulan = 3;
var tahun = 1990;

switch(bulan) {
     case 1:
         bulan = "Januari";
     break;
     case 2:
         bulan = "Februari";
     break;
     case 3:
         bulan = "Maret";
     break;
     case 4:
         bulan = "April";
     break;
     case 5:
         bulan = "Mei";
     break;
     case 6:
         bulan = "Juni";
     break;
     case 7:
         bulan = "Juli";
     break;
     case 8:
         bulan = "Agustus";
     break;
     case 9:
         bulan = "September";
     break;
     case 10:
         bulan = "Oktober";
     break;
     case 11:
         bulan = "November";
     break;
     case 12:
         bulan = "Desember";
     break;
}
console.log(tanggal+" "+bulan+" "+tahun);
console.log ('-------------------------');